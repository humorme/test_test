import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


from decimal import Decimal

import sys
sys.setrecursionlimit(1000)

import re


class EntryWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="Silnia + NWW + NWD + Dwójkowo")
        self.set_size_request(200, 100)
        self.set_border_width(10)
        self.set_resizable(False)

        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        self.add(vbox)

        self.entry = Gtk.Entry()
        self.entry.set_text('Wprowadź liczby')
        vbox.pack_start(self.entry, True, True, 0)
        vbox.set_size_request(200, 100)

        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        vbox.pack_start(hbox, True, True, 0)

        button1 = Gtk.Button.new_with_label("Silnia")
        button1.connect("clicked", self.silnia)
        button1.set_tooltip_text('Podaj liczbę')

        button2 = Gtk.Button.new_with_label("NWD i NWW")
        button2.connect("clicked", self.NWD_NWW)
        button2.set_tooltip_text('Podaj dwie liczby oddzielone przecinkiem')

        button5 = Gtk.Button.new_with_label("Wyczyść")
        button5.connect("clicked", self.clear)
        button5.set_tooltip_text('wyczyść')

        button4 = Gtk.Button.new_with_label("Dwójkowo")
        button4.connect("clicked", self.to_binary)
        button4.set_tooltip_text('binary')

        button3 = Gtk.Button.new_with_label("EGCD")
        button3.connect("clicked", self.egcd)
        button3.set_tooltip_text('egcd')

        button6 = Gtk.Button.new_with_label("Dziesiętnie")
        button6.connect("clicked", self.to_decimal)
        button6.set_tooltip_text('Podaj liczbę w zapisie dwójkowym')

        scrollable = Gtk.ScrolledWindow()
        scrollable.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        vbox.pack_start(scrollable, True, True, 0)

        self.label = Gtk.Label()
        self.label.set_selectable(True)
        # self.label.set_width_chars(50)
        # self.label.set_line_wrap(True)
        # self.label.set_size_request(200,100)
        scrollable.add(self.label)

        hbox.pack_start(button1, True, True, 0)
        hbox.pack_start(button2, True, True, 0)
        hbox.pack_start(button3, True, True, 0)
        hbox.pack_start(button4, True, True, 0)
        hbox.pack_start(button5, True, True, 0)
        hbox.pack_start(button6, True, True, 0)

        vbox1 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        vbox.pack_start(vbox1, True, True, 0)

        self.label1 = Gtk.Label()
        self.label1.set_selectable(True)
        vbox1.pack_start(self.label1, True, True, 0)

    # def test(self,button1):
    #   self.label.set_text(self.entry.get_text())
    #  print(self.label.get_text())

    def clear(self, *args):
        # czyszczenie wejścia i wyjścia
        self.entry.set_text(str())
        self.label.set_text(str())
        self.label1.set_text(str())

    def NWD_NWW(self, *args):

     try:
        input = self.entry.get_text()
        print(input)
        input_list = input.split(',')

        x = int(input_list[0])
        print(x)
        y = int(input_list[1])
        print(y)

        xy = [x, y]
        print(xy)

        rezultat = []

        while True:
            z = divmod(x, y)
            rezultat.append(z)
            print(rezultat)

            if z[1] == 0:
                try:
                    v = rezultat[-2]
                    # print('NWD wynosi ' + str(v[1]))
                    self.label.set_text('NWD wynosi ' + str(v[1]))
                    break

                except IndexError:
                    v = rezultat[-1]
                    # print('NWD wynosi ' + str(y))
                    self.label.set_text('NWD wynosi ' + str(y))
                    break
            else:
                x = y
                y = z[1]
        try:
            nww = (xy[0] * xy[1]) / v[1]
            # print('NWW wynosi ' + str(nww))
            self.label1.set_text('NWW wynosi ' + str(nww))

        except ZeroDivisionError:
            # print('NWW wynosi ' + str(x))
            self.label1.set_text('NWW wynosi ' + str(x))

     except  (ValueError, IndexError) as e:
        self.label.set_text('Podaj dwie liczby oddzielone przecinkiem')
        self.label1.set_text(str())

    def silnia(self, x):

        try:
            self.label1.set_text(str())
            # print(self.label.get_width_chars())
            x = int(self.entry.get_text())

            x > 1
            z = x - 1
            while z > 1:
                x = x * z
                z = z - 1

            if len(str(x)) > 1000:
                return self.label.set_text('Silnia wynosi: {:.10e}'.format(Decimal(str(x))))
            else:
                return self.label.set_text('Silnia wynosi:  ' + str(x))

        except ValueError:
            self.label.set_text(str('Podaj liczbę'))
            self.label1.set_text(str())


    def to_binary(self, *args):
        try:
            y = int(self.entry.get_text())
            self.label1.set_text(str())
            self.binary = []

            while y > 0:
                res = divmod(y, 2)
                y = res[0]
                self.binary.append(res[1])
            # print(binary)
            rev = []
            rev_index = len(self.binary) - 1
            while rev_index > -1:
                self.binary[rev_index]
                rev.append(self.binary[rev_index])
                rev_index = rev_index - 1
            revs = [str(g) for g in rev]  # string out of -->>list part 1
            revs_1 = ''.join(revs)
            return self.label.set_text(revs_1)

        except ValueError:
            self.label.set_text(str('Podaj liczbę'))
            self.label1.set_text(str())



    # def get_numbers(self, *args):
    #     self.g = self.entry.get_text()
    #     print(self.g)

    def egcd(self, *args):
      try:
        input = self.entry.get_text()
        print(input)
        input_list = input.split(',')

        a = int(input_list[0])
        print(a)
        b = int(input_list[1])
        print(b)

        x0, x1, y0, y1 = 0, 1, 1, 0
        m = b
        while a != 0:
            (q, a), b = divmod(b, a), a
            print(q, a, b)
            y0, y1 = y1, y0 - q * y1
            x0, x1 = x1, x0 - q * x1
        self.label.set_text("NWD wynosi " + str(b))
        print(b,x0,y0)
        if b!=1:
            self.label1.set_text("Nie ma odwrtoności modulo")

        else:
            if x0 >0:
                self.label1.set_text("Odwrotność modulo pierwszej liczby wynosi " +str(x0))
            else:
                self.label1.set_text("Odwrotność modulo pierwszej liczby wynosi " + str(x0+m))
      except (ValueError, IndexError) as e:
                self.label.set_text('Podaj dwie liczby oddzielone przecinkiem')

    def to_decimal(self, *args):
        try:
            suma = 0
            y = str(self.entry.get_text())
            if re.search('[23456789]', y):
                raise ValueError
            # if '2' in y:
            #     raise ValueError
            # if '3' in y:
            #     raise ValueError
            # if '4' in y:
            #     raise ValueError
            # if '5' in y:
            #     raise ValueError
            # if '6' in y:
            #     raise ValueError
            # if '7' in y:
            #     raise ValueError
            # if '8' in y:
            #     raise ValueError
            # if '9' in y:
            #     raise ValueError
            if re.search('[A-z]', self.entry.get_text()):
                raise ValueError
            if re.search('[\W]', self.entry.get_text()):
                raise ValueError
            else:
                g = len(y) - 1
                for digit in y:
                    if digit == str(1):
                        t = 2 ** g
                        print(g)
                        suma = suma + t
                # print(suma)
                    else:
                        pass
                    g = g - 1

            return self.label.set_text(str(suma))

        except ValueError:
            self.label.set_text('To nie jest zapis dwójkowy')


win = EntryWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
