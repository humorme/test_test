import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

import re

from decimal import Decimal


import matplotlib.pylab as plt

import matplotlib.pyplot

matplotlib.pyplot.ion()

class TooLong(Exception):
    pass

class GridWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="Calculator")
        self.set_size_request(200, 100)
        self.set_border_width(10)
        self.set_resizable(False)

        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.add(vbox)
        #vbox.set_size_request(200, 100)
        self.entry = Gtk.Entry()
        self.entry.set_text('Provide your numbers')
        vbox.pack_start(self.entry, True, True, 0)

        grid = Gtk.Grid()
        grid.set_row_spacing(6)
        grid.set_column_spacing(6)

        #self.add(grid)
        vbox.pack_start(grid, True, True, 0)


        button1 = Gtk.Button(label="Luhn")
        button1.connect('clicked', self.luhn)

        button2 = Gtk.Button(label="Factorial")
        button2.connect('clicked', self.silnia)

        button3 = Gtk.Button(label="GCD and LCM")
        button3.connect('clicked', self.NWD_NWW)

        button4 = Gtk.Button(label="EGCD")
        button4.connect('clicked', self.egcd)

        button5 = Gtk.Button(label="to_Binary")
        button5.connect('clicked', self.to_binary)

        button6 = Gtk.Button(label="to_Decimal")
        button6.connect('clicked', self.to_decimal)

        button8 = Gtk.Button(label='Clear')
        button8.connect('clicked', self.clear)

        button7 = Gtk.Button(label="3N+1")
        button7.connect('clicked', self.Collatz)

        grid.add(button1)
        grid.attach(button2, 1, 0, 1, 1)
        grid.attach(button3, 2, 0, 1, 1)
        grid.attach(button4, 0, 1, 1, 1)
        grid.attach(button5, 1, 1, 1, 1)
        grid.attach(button6, 2, 1, 1, 1)
        grid.attach(button7, 0, 2, 3, 1)
        grid.attach(button8, 0, 3, 3, 1)

        scrollable = Gtk.ScrolledWindow()
        scrollable.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        vbox.pack_start(scrollable, True, True, 0)

        self.label = Gtk.Label()
        self.label.set_selectable(True)
        scrollable.add(self.label)

        self.label1 = Gtk.Label()
        vbox.pack_start(self.label1, True, True, 0)

    def clear(self, *args):
        self.entry.set_text(str())
        self.label.set_text(str())
        self.label1.set_text(str())

    def silnia(self, x):

        self.label1.set_text(str())

        try:
            if len(self.entry.get_text()) > 5 and int(self.entry.get_text()):
                raise TooLong
            #self.label1.set_text(str())
            # print(self.label.get_width_chars())
            x = int(self.entry.get_text())

            x > 1
            z = x - 1
            while z > 1:
                x = x * z
                z = z - 1

            if len(str(x)) > 1000:
                return self.label.set_text('Factorial: {:.10e}'.format(Decimal(str(x))))
            else:
                return self.label.set_text('Factorial:  ' + str(x))

        except ValueError:
            self.label.set_text(str('Provide a number'))
            #self.label1.set_text(str())

        except TooLong:
            self.label.set_text('Number is too big')

    def luhn(self, *args):

        self.label1.set_text(str())



        try:

            y = self.entry.get_text()

            suma1 = 0

            suma2 = 0

            if len(y) == 0:
                raise ValueError

            if len(y) % 2 == 0:
                z = y[1::2]
                v = y[::2]
            else:
                z = y[::2]
                v = y[1::2]

            for i in z:
                if int(i) == 5:
                    j = 1
                    suma1 = suma1 + j
                elif int(i) == 6:
                    k = 3
                    suma1 = suma1 + k
                elif int(i) == 7:
                    l = 5
                    suma1 = suma1 + l
                elif int(i) == 8:
                    m = 7
                    suma1 = suma1 + m
                elif int(i) == 9:
                    n = 9
                    suma1 = suma1 + n
                else:
                    suma1 = suma1 + int(i) * 2

            # print(suma1)

            for i in v:
                suma2 = suma2 + int(i)

            # print(suma2)

            suma3 = suma2 + suma1

            # print(suma3)

            a = suma3 % 10

            if a == 0:
                return self.label.set_text(str(y)+ '_' + str(0))

            else:
                b = 10 - a
                return self.label.set_text(str(y)+ '_' + str(b))

        except ValueError:
                self.label.set_text('Provide a number ')



    def NWD_NWW(self, *args):

     try:
        input = self.entry.get_text()
        print(input)
        input_list = input.split(',')

        x = int(input_list[0])
        print(x)
        y = int(input_list[1])
        print(y)

        xy = [x, y]
        print(xy)

        rezultat = []

        while True:
            z = divmod(x, y)
            rezultat.append(z)
            print(rezultat)

            if z[1] == 0:
                try:
                    v = rezultat[-2]
                    # print('NWD wynosi ' + str(v[1]))
                    self.label.set_text('GCD is ' + str(v[1]))
                    break

                except IndexError:
                    v = rezultat[-1]
                    # print('NWD wynosi ' + str(y))
                    self.label.set_text('LCM is ' + str(y))
                    break
            else:
                x = y
                y = z[1]
        try:
            nww = (xy[0] * xy[1]) / v[1]
            # print('NWW wynosi ' + str(nww))
            self.label1.set_text('LCM is ' + str(nww))

        except ZeroDivisionError:
            # print('NWW wynosi ' + str(x))
            self.label1.set_text('LCM is ' + str(x))

     except  (ValueError, IndexError) as e:
        self.label.set_text('Provide two numbers, separated by comma')
        self.label1.set_text(str())


    def egcd(self, *args):
      try:
        input = self.entry.get_text()
        print(input)
        input_list = input.split(',')

        a = int(input_list[0])
        print(a)
        b = int(input_list[1])
        print(b)

        x0, x1, y0, y1 = 0, 1, 1, 0
        m = b
        while a != 0:
            (q, a), b = divmod(b, a), a
            print(q, a, b)
            y0, y1 = y1, y0 - q * y1
            x0, x1 = x1, x0 - q * x1
        self.label.set_text("GCD is " + str(b))
        print(b,x0,y0)
        if b!=1:
            self.label1.set_text("Modulo inverse does not exist")

        else:
            if x0 >0:
                self.label1.set_text("Mmdulo inverse of first number is " +str(x0))
            else:
                self.label1.set_text("Modulo inverse of first number is " + str(x0+m))
      except (ValueError, IndexError) as e:
                self.label.set_text('Provide two numbers, separated by comma')


    def to_binary(self, *args):
        try:
            y = int(self.entry.get_text())
            self.label1.set_text(str())
            self.binary = []

            while y > 0:
                res = divmod(y, 2)
                y = res[0]
                self.binary.append(res[1])
            # print(binary)
            rev = []
            rev_index = len(self.binary) - 1
            while rev_index > -1:
                self.binary[rev_index]
                rev.append(self.binary[rev_index])
                rev_index = rev_index - 1
            revs = [str(g) for g in rev]  # string out of -->>list part 1
            revs_1 = ''.join(revs)
            return self.label.set_text(revs_1)

        except ValueError:
            self.label.set_text(str('Provide a number'))
            self.label1.set_text(str())



    def to_decimal(self, *args):
        try:
            suma = 0
            y = str(self.entry.get_text())

            if len(y) == 0:
                raise ValueError

            if re.search('[23456789]', y):
                raise ValueError
            # if '2' in y:
            #     raise ValueError
            # if '3' in y:
            #     raise ValueError
            # if '4' in y:
            #     raise ValueError
            # if '5' in y:
            #     raise ValueError
            # if '6' in y:
            #     raise ValueError
            # if '7' in y:
            #     raise ValueError
            # if '8' in y:
            #     raise ValueError
            # if '9' in y:
            #     raise ValueError
            if re.search('[A-z]', self.entry.get_text()):
                raise ValueError
            if re.search('[\W]', self.entry.get_text()):
                raise ValueError
            else:
                g = len(y) - 1
                for digit in y:
                    if digit == str(1):
                        t = 2 ** g
                        print(g)
                        suma = suma + t
                # print(suma)
                    else:
                        pass
                    g = g - 1

            return self.label.set_text(str(suma))

        except ValueError:
            self.label.set_text('It is not a binary mode number')

    def Collatz(self, *args):

        y = []
        z = []


        try:

                x = int(self.entry.get_text())
                print(x)

                if x:
                    y.append(x)
                    self.label.set_text('')
                if x == 0:
                    self.label.set_text('Provide a number other than 0 ')

        except ValueError:
                   self.label.set_text('This is not a number')
                   #self.entry.set_text(str())

        while True:
            try:
                if x % 2 == 0:
                    x = int(x / 2)
                    y.append(x)

                elif x == 0:
                    break

                else:
                    x = int(3 * x + 1)
                    y.append(x)
                    print(x)

                if x == 1:
                    break
                if x == 0:
                    break

            except UnboundLocalError:
                break
        enum = enumerate(y)

        for indices, values in enum:
            print(indices, values)
            z.append(indices)
        g = zip(z, y)
        f = dict(g)
        print(f)

        if y and x != 0:
            plt.plot(z, y)
        plt.show(block=False)




win = GridWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()