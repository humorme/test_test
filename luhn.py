#Luhn alogorithm

def luhn(x):
    y = str(x)

    suma1 = 0

    suma2 = 0

    if len(y) % 2 == 0:
        z = y[1::2]
        v = y[::2]
    else:
        z = y[::2]
        v = y[1::2]

    for i in z:
        if int(i) == 5:
            j = 1
            suma1 = suma1 + j
        elif int(i) == 6:
            k = 3
            suma1 = suma1 + k
        elif int(i) == 7:
            l = 5
            suma1 = suma1 + l
        elif int(i) == 8:
            m = 7
            suma1 = suma1 + m
        elif int(i) == 9:
            n = 9
            suma1 = suma1 + n
        else:
            suma1 = suma1 + int(i) * 2

    #print(suma1)

    for i in v:
            suma2 = suma2 + int(i)

    #print(suma2)

    suma3 = suma2 +suma1

    #print(suma3)

    a = suma3 % 10

    if a == 0:
        return 0

    else:
        return 10 - a

print(luhn(89023456123212383))