from decimal import Decimal

#silnia iteracyjna?

def silnia(x):
    x > 1
    z = x - 1
    while z > 1:
        x = x * z
        z = z - 1
    return x


a = silnia(12)


if len(str(a)) > 1000:
    print('{:.10e}'.format(Decimal(str(a))))
else:
    print(str(a))

#g =str(a)

#len(g)

#print(g)

#print(len(g))

f = (2, 2, 2, 2)


#silnia rekurencyjna

def silnia_rek(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return silnia_rek(n-1)*n



def mnozenie(y):
    o = y[0]
    n = len(y) - 1
    while n > 0:
        o = o * y[n]
        n = n - 1
    return o


b = mnozenie(f)

#print(b)

print("Silnia rekurencyjna wynosi: " + str(silnia_rek(6)))